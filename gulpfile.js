'user strict'

var gulp = require('gulp'),
    sass = require('gulp-sass');
    browserSync = require('browser-sync');
    del = require('del');
    imagemin = require('gulp-imagemin');
    uglify = require('gulp-uglify');
    usermin = require('gulp-usermin');
    rev = require('gulp-rev');
    cleanCss = require('gulp-clean-css');
    flatmap = require('gulp-flatmap');
    htmlmin = require('gulp-htmlmin');
    
const browserSync = require('browser-sync');
const imagemin = require('gulp-imagemin');

gulp.task('sass', function(){
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));      
})

gulp.task('sass:watch', function(){
    gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function(){
    var files = ['./*.html', './css/*.css', './img/*.{png, jpg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

